function createList(array, parentElement = document.body) {
    let list = document.createElement('ul');
    parentElement.appendChild(list);
    array.forEach(element => {
    if (!Array.isArray(element)) {
      const listItem = document.createElement("li");
      listItem.innerText = element;
      list.appendChild(listItem)}
      else {
      createList(element, list)}});
    
    return parentElement.appendChild(list);
  }
let timeleft = 2;
let downloadTimer = setInterval(function(){
  if(timeleft <= 0){
    clearInterval(downloadTimer);
    document.getElementById("countdown").innerHTML = "Finished";
  } else {
    document.getElementById("countdown").innerHTML = timeleft + " seconds to clear the document";
  }
  timeleft -= 1;
}, 1000);
setTimeout(() => document.body.innerHTML = "", 3000);


createList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
createList(["1", "2", "3", "sea", "user", 23]);
createList(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"])
createList(["Madrid", ["Adolfo Suárez Madrid-Barajas", "Cuatro Vientos"], "Barcelona", "Tenerife", ["Tenerife Sur", "Tenerife Norte-Ciudad de La Laguna"], "Malaga"]); 